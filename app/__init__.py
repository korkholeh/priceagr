from flask import Flask
from flask.ext import restful
from flask.ext.mongoengine import MongoEngine

app = Flask(__name__)
app.config.from_object('config')

from app.api.views import mod as apiModule
app.register_blueprint(apiModule)

db = MongoEngine(app)

api = restful.Api(app)
from app.api.resources import (
    TaskResource,
    BotResource,
    ClientResource,
    ProductResource,
)

api.add_resource(TaskResource, '/task')
api.add_resource(BotResource, '/bot')
api.add_resource(ClientResource, '/client')
api.add_resource(ProductResource, '/products/<string:locator>')

# Global views


@app.route('/')
def index():
    return 'Please use API'
