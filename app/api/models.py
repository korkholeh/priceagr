# coding: utf-8
from app import db


class Price(db.EmbeddedDocument):
    source_domain = db.StringField(max_length=200, required=True)
    price = db.DecimalField(precision=2)
    currency = db.StringField(max_length=10)


class Product(db.Document):
    locator = db.StringField(max_length=300, required=True, unique=True)
    human_name = db.StringField(max_length=300, required=True)
    prices = db.ListField(db.EmbeddedDocumentField('Price'))
    urls_for_parsing = db.ListField(db.URLField())

    def __unicode__(self):
        return self.human_name

    meta = {
        'indexes': ['locator'],
    }
