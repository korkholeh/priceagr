# coding: utf-8
from flask.ext import restful
from .models import Product


class TaskResource(restful.Resource):
    def get(self):
        return {'hello': 'task'}


class ClientResource(restful.Resource):
    def get(self):
        return {'hello': 'client'}


class BotResource(restful.Resource):
    def get(self):
        return {'hello': 'bot'}


class ProductResource(restful.Resource):
    def get(self, locator):
        product = Product.objects.get_or_404(locator=locator)
        print product.to_mongo()
        return {
            'locator': product.locator,
            'human_name': product.human_name,
            'prices': [{
                'price': float(p.price),
                'currency': p.currency,
                'source_domain': p.source_domain,
            } for p in product.prices],
            'urls_for_parsing': product.urls_for_parsing,
        }
