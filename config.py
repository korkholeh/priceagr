# coding: utf-8
import os
_basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = False

ADMINS = frozenset(['korkholeh@gmail.com'])

MONGODB_SETTINGS = {'DB': 'priceagrs'}
